import React, { Component } from 'react'
import { connect } from 'react-redux';
import Error from '../../template/Error';

 class TimerCount extends Component<any,any> {
    
    constructor(props)
    {
        super(props);
        this.state = {
            data : {},
            error : false, 
            error_data : {}
        }
    }

    componentDidMount()
    {
   // console.log(this.props.starts);
    }

   
}



const mapStateToProps = (state) => {
    return {
      user : state.user,
      data : state.data
    }  
  }
  
  const mapDispatchToProps = (dispatch) =>{
      return { 
        updateData : (data)=>{dispatch({type:'APPEND', payload : data })}
      }
  }

  export default connect(mapStateToProps,mapDispatchToProps)(TimerCount)
