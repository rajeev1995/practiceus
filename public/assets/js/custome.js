$(document).ready(function() {
    var owl = $('.owl-carousel2');
    owl.owlCarousel({
      dots: true,
      loop: true,
      margin: 10,
      singleItem: true,
      autoPlay : false,
      stopOnHover : true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      responsive:{
        768:{
            items:2,
            nav:true,
            loop:true
        },
        1024:{
          items:3,
          nav:true,
          loop:true
      },
      1200:{
          items:3,
          nav:true,
          loop:true
      }
    }
});


    var owl2 = $('.owl-carousel3');
    owl2.owlCarousel({
      dots: true,
      loop: true,
      margin: 10,
      singleItem: true,
      autoPlay : false,
      stopOnHover : true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      responsive:{
        768:{
            items:2,
            nav:true,
            loop:true
        },
        1024:{
          items:3,
          nav:true,
          loop:true
      },
      1200:{
          items:4,
          nav:true,
          loop:true
      }
    }
});

    
});

//  Menu 
$(document).ready(function(){
	$(".navbar-toggler").click(function(){
		$(".menu-show").addClass("show-menu")
	});
	$(".menu-show .close-menu").click(function(){
		$(".menu-show").removeClass("show-menu")
	});
});


